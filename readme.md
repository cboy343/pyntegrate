# Integration Approximation App using Riemann Sums, Trapezoid Rule, and Simpson's Rule

This Python3 based application can calculate definite integrals in terms of x using Riemann Sums, Trapezoid Rule, or Simpson's Rule. The user defines the sub-intervals 'n' for the calculation. 

This application was made as a challenge for maths class that I spent roughly 8 hours working on, so it may be a bit buggy. I'm probably not going to work on it anymore so feel free to make pull requests and improve upon it.

This application uses a modified [fourFn.py](https://stackoverflow.com/questions/2371436/evaluating-a-mathematical-expression-in-a-string) to handle the user inputs. The program was made using Python 3.7.3 and Tkinter. In the virtual environment, the only things installed are pyparsing, pyinstaller (in case anyone wants to package it up as a standalone executable), and pylint.