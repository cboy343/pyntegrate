"""
This is a program that will integrate a function with respect
to x using either Riemann sums, Simpsons rule, or Trapezium rule.

Author: Cameron Mayes

Dependencies: pyparsing
"""

from tkinter import *
from tkinter import messagebox
from fourFn import NumericStringParser
from sys import exit
from tkinter.ttk import *


class Gui(object):
	"""This class initiates the GUI and sets off the appropriate
	class"""
	def __init__(self, window):
		"""Initiates the GUI"""
		window.rowconfigure((0,5), weight=1)  # make buttons stretch when
		window.columnconfigure((0,2), weight=1)
		window.title("Pyntegrate")
		self.upper = Entry(window, width=3)
		self.upper.grid(row=0, column=0, padx=(20, 0), pady=(5,0))
		self.integral = Label(window, text="\u222B", font=('Arial', 15))
		self.integral.grid(row=1, column=0, sticky="W", padx=(20, 0))
		self.equation = Entry(window, width=40)
		self.equation.grid(row=1, column=1, columnspan=3, sticky="we")
		self.dx = Label(window, text="dx", font=('Arial', 15))
		self.dx.grid(row=1, column=4, sticky="E", padx=(0, 20))
		self.lower = Entry(window, width=3)
		self.lower.grid(row=2, column=0, padx=(20, 0))
		self.sub_intervals_label = Label(window, text="n = ", font=('Arial', 15))
		self.sub_intervals_label.grid(row=3, column=0, padx=(20, 0))
		self.sub_intervals = Entry(window, width=5)
		self.sub_intervals.grid(sticky='W', row=3, column=1) 
		integral_types = ['Riemann Sum', 'Trapezium Rule', "Simpson's rule"]
		self.type = Combobox(window, values=integral_types, font=('Arial', 15), width=15, state="readonly")
		self.type.grid(row=3, column=3, columnspan=2, padx=(0, 20))
		self.type.set("Riemann Sum")
		self.submit = Button(window, text="Integrate!", command=self.click_handler)
		self.submit.grid(row=4, column=0, columnspan=5, pady=(5,0))
		self.result = Label(window, font=('Arial', 15))
		self.result.grid(row=5, column=0, columnspan=5, pady=(5,0))
		
	def click_handler(self):
		"""Calls the appropriate class to begin integrating"""
		type = self.type.get()
		equation = self.equation.get()
		lower = self.lower.get()
		upper = self.upper.get()
		intervals = self.sub_intervals.get()
		integral = Integration(equation, upper, lower, intervals)
		if type == "Riemann Sum":
			result = integral.riemann()	
		elif type == "Trapezium Rule":
			result = integral.trapezoid()
		elif type == "Simpson's rule":
			result = integral.simpson()
		
		self.result['text'] = "{:.3f} units\u00B2".format(float(result))
		
class Integration(object):
	"""Integrates function using Trapezium Sums"""
	
	def __init__(self, equation, upper, lower, intervals):
		"""Initialises the integral"""
		self.equation = equation
		self.upper = upper
		self.lower = lower
		self.intervals = intervals
		
	def replace_str(self, string, x_value):
		"""Replaces equation string with math"""
		changed_string =  string.replace('x', '({})'.format(str(x_value)))
		changed_string = changed_string.replace('e', 'E')
		changed_string = changed_string.replace('ln', 'log')
		try:
			nsp = NumericStringParser()
			changed_string = nsp.eval(changed_string)
			changed_string = float(changed_string)
			return changed_string
		except NameError:
			messagebox.showerror("Error", "Invalid equation. Code: NameError")
		except TypeError:
			messagebox.showerror("Error", "Invalid equation. Code: TypeError")
		except SyntaxError:
			messagebox.showerror("Error", "Invalid equation. Code: SyntaxError")
		except pyp.ParseException:
			messagebox.showerror("Error", "Invalid equation. Code: Pyp Parse Error")		
		except ValueError:
			messagebox.showerror("Error", "Incorrect domain for function")
		except ZeroDivisionError:
			messagebox.showerror("Error", "Please use a proper integral")
		exit(1)
	
	def check_inputs(self):
		"""This function checks to see if the user has put in the wrong values"""
		try:
			nsp = NumericStringParser()
			intervals  = abs(int(self.intervals))
			upper  = float(nsp.eval(self.upper))
			lower  = float(nsp.eval(self.lower))
			if upper <= lower:
				messagebox.showerror("Error", "Upper limit must be bigger than lower limit")
				exit(1)
			return intervals, upper, lower
		except ValueError:
			messagebox.showerror("Error", "Please type in an integer for n.")
		except pyp.ParseException:
			messagebox.showerror("Error", "Please type in an integer for n.")
		exit(1)
			
	def trapezoid(self):
		"""Integrates the function using the trapezoid rule"""	
		intervals, upper, lower = self.check_inputs()
		equation = self.equation
		delta_x = (upper - lower) / intervals
		x_naught = self.replace_str(equation, lower)
		area_sum = x_naught
		for i in range(intervals - 1):
			x_value = lower + delta_x * (i + 1)
			current_equation = self.replace_str(equation, x_value)
			area_sum += 2 * current_equation
		current_equation = self.replace_str(equation, upper)
		area_sum += current_equation
		area_sum = (delta_x / 2) * area_sum
		return area_sum
	
	def riemann(self):
		"""Integrates the function using Riemann sums"""
		intervals, upper, lower = self.check_inputs()
		equation = self.equation
		delta_x = (upper - lower) / intervals
		area_sum = 0
		for i in range(intervals):
			x_value = lower + (i * delta_x)
			current_equation = self.replace_str(equation, x_value)
			current_equation = current_equation * delta_x
			area_sum += current_equation
		return area_sum
	
	def simpson(self):
		""" Integrates the function using Simpson's rule"""
		intervals, upper, lower = self.check_inputs()
		equation = self.equation
		delta_x = (upper - lower) / intervals	
		area_sum = self.replace_str(equation, lower) + self.replace_str(equation, upper)
		for i in range(intervals - 1):
			current = i + 1
			x_value = lower + delta_x * current
			if current % 2 == 0:
				area_sum += 2 * self.replace_str(equation, x_value)
			elif current % 2 != 0:
				area_sum += 4 * self.replace_str(equation, x_value)
		area_sum = area_sum * (delta_x / 3)
		return area_sum

def main():
	"""Starts the program up"""
	window = Tk()
	gui = Gui(window)
	window.configure(background="white")
	window.resizable(False, False)
	window.mainloop()
main()
